def blackjack_hand_greater_than(hand_1, hand_2):
    """
    Return True if hand_1 beats hand_2, and False otherwise.
    
    In order for hand_1 to beat hand_2 the following must be true:
    - The total of hand_1 must not exceed 21
    - The total of hand_1 must exceed the total of hand_2 OR hand_2's total must exceed 21
    
    Hands are represented as a list of cards. Each card is represented by a string.
    
    When adding up a hand's total, cards with numbers count for that many points. Face
    cards ('J', 'Q', and 'K') are worth 10 points. 'A' can count for 1 or 11.
    
    When determining a hand's total, you should try to count aces in the way that 
    maximizes the hand's total without going over 21. e.g. the total of ['A', 'A', '9'] is 21,
    the total of ['A', 'A', '9', '3'] is 14.
    
    Examples:
    >>> blackjack_hand_greater_than(['K'], ['3', '4'])
    True
    >>> blackjack_hand_greater_than(['K'], ['10'])
    False
    >>> blackjack_hand_greater_than(['K', 'K', '2'], ['3'])
    False
    """
    
    
    vocab = {}
    vocab['J'] = 10
    vocab['Q'] = 10
    vocab['K'] = 10
    
    
    def calc_score_with_aces(hand):
        aces = 0
        sumA = 0
        for card in hand:
            if card == 'A':
                aces+=1
            else:
                if card in vocab:
                    sumA+= vocab[card]
                else:
                    sumA+= int(card)
        sumA+= aces

        while (aces > 0):
            if(sumA + 10 < 21):
                sumA += 10
                aces -= 1
            else:
                break
        return sumA
    
    
    
    def get_best_hand_count(hand):
        sumA = 0
        if 'A' in hand:
            sumA = calc_score_with_aces(hand)
        else:
            for card in hand:
                if card in vocab:
                    sumA += vocab[card]
                else:
                    sumA += int(card)
        return sumA
    
    sum1, sum2 = get_best_hand_count(hand_1), get_best_hand_count(hand_2)
    if sum1 > 21 : return False
    if sum2 > 21 : return True
    return sum1 > sum2

